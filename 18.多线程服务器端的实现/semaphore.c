#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>


// 函数声明
void *recive(void *arg);
void *accu(void *arg);


sem_t sem_one,sem_two;
int num = 0;


int main(){

    pthread_t tid1, tid2;
    // 初始化信号量
    sem_init(&sem_one, 0, 0);
    sem_init(&sem_two, 0, 1);

    // 创建线程
    pthread_create(&tid1, NULL, recive, NULL);
    pthread_create(&tid2, NULL, accu, NULL);

    // 回收线程
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);

    return 0;
}

void *recive(void *arg){
    for (size_t i = 0; i < 5; i++){
        fputs("Input num: ",stdout);
        sem_wait(&sem_two);
        scanf("%d",&num);
        sem_post(&sem_one);
    }
    return NULL;
}

void *accu(void *arg){
    int sum = 0;
    for (size_t i = 0; i < 5; i++){
        sem_wait(&sem_one);
        sum = sum + num;
        sem_post(&sem_two);
    }

    printf("sum is [%d]\n",sum);
    return NULL;
}