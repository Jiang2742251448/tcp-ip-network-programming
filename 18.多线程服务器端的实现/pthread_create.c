#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void *thread_callback(void *arg){

    printf("thread ... \n");
    sleep(2);
    return NULL;
}

int main(){

    // 创建一个子线程
    pthread_t tid;
    if(pthread_create(&tid, NULL, thread_callback, NULL)){
        fprintf(stderr, "pthread_create error\n");
        exit(-1);
    }

    sleep(1);
    puts("end of main\n");


    return 0;
}