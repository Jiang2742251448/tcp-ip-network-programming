#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void *thread_callback(void *arg){

    printf("thread ... \n");
    sleep(2);

    int *a = malloc(sizeof(int));
    *a = 2;

    return (void *)a;
}

int main(){

    // 创建一个子线程
    pthread_t tid;
    if(pthread_create(&tid, NULL, thread_callback, NULL)){
        fprintf(stderr, "pthread_create error\n");
        exit(-1);
    }

    int *a = NULL;
    if(pthread_join(tid, (void **)&a)){
        fprintf(stderr, "pthread_join error\n");
        exit(-1);
    }  

    printf("the pthread return value is: %d\n",*a);

    // 注意：a是在回调函数中分配了空间，所以要在这里释放
    if(a != NULL){
        free(a);
        a = NULL;
    }  

    puts("end of main\n");
    return 0;
}