#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int sum = 0;

void *thread_callback(void *arg){

    int start = ((int *)arg)[0];
    int end = ((int *)arg)[1];
    while (start <= end){
        sum = sum + start;
        ++start;
    }
    
    return NULL;
}

int main(){

    pthread_t tid1, tid2;
    int arr1[] = {1,5};
    int arr2[] = {6,10};
    if (pthread_create(&tid1, NULL, thread_callback, (void *)arr1)){
        fprintf(stderr, "pthread_create error\n");
        exit(-1);
    }

    if (pthread_create(&tid2, NULL, thread_callback, (void *)arr2)){
        fprintf(stderr, "pthread_create error\n");
        exit(-1);
    }
    

    if(pthread_join(tid1, NULL)){
        fprintf(stderr, "pthread_join error\n");
        exit(-1);
    }  

    if(pthread_join(tid2, NULL)){
        fprintf(stderr, "pthread_join error\n");
        exit(-1);
    }  
    
    printf("sum = [%d]\n",sum);

    puts("end of main\n");
    return 0;
}