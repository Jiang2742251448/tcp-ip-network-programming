#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(){

    // 打开文件
    int fd = open("./output.log",O_RDWR | O_CREAT,0640);
    if (fd < 0){
        fprintf(stderr,"open error\n");
        exit(-1); // 255
    }

    // 写入
    char buf[1024];
    memset(buf,'\0',sizeof(buf));
    if(read(fd,buf,sizeof(buf)) == -1){
        fprintf(stderr,"read error\n");
        exit(-1);
    }
    printf("%s\n",buf);

    // 关闭
    close(fd);
    return 0;
}