#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>

int main(){
    char str[] = "what are you doing";
    char buf[1024];

    int fds[2];

    if(pipe(fds) < 0){
        fprintf(stderr,"pipe error\n");
        exit(-1);
    }

    pid_t pid = fork();
    if (pid < 0){
        fprintf(stderr,"fork error\n");
        exit(-1);
    }

    if (pid == 0){
        // child写，所以需要关闭读端
        close(fds[0]);
        write(fds[1],str,sizeof(str));
    }else{
        // parent读，所以关闭写段
        close(fds[1]);
        memset(buf,'\0',sizeof(buf));
        read(fds[0],buf,sizeof(buf));
        printf("%s\n",buf);
    }
    
    return 0;
}