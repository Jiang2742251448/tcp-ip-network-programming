#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>

void read_childproc(int sig){
    int status;
    pid_t ret = waitpid(-1,&status,WNOHANG);
    if (WIFEXITED(status)){
        printf("child [%d] return [%d]\n",ret,WEXITSTATUS(status));
    }
    
}

int main(){

    struct sigaction act;
    act.sa_handler = read_childproc;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGCHLD,&act,0);

    pid_t pid = fork();
    if (pid < 0){
        fprintf(stderr,"fork error\n");
        exit(-1);
    }

    if(pid == 0){ // child
         puts("child ... \n");
         sleep(10);
         return 12;
    }else{ // parent
        pid_t pid2 = fork();
        if(pid2 < 0){
            fprintf(stderr,"fork error\n");
            exit(-1);
        }
        if(pid2 == 0){
            puts("child ... \n");
            sleep(10);
            exit(24);
        }else{
            for (size_t i = 0; i < 5; i++){
                puts("wait...\n");
                sleep(5);
            }
        }
    }

    return 0;
}