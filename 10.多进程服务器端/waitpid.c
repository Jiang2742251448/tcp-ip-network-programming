#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(){

    pid_t pid = fork();
    if(pid < 0){
        fprintf(stderr,"fork error!\n");
        exit(1);
    } 

    if(pid == 0){ // child
        sleep(10);
        return 3;
    }else{ // parent
        int status;
        // WNOHANG：即使没有子进程终止，也不会阻塞
        pid_t ret;
        while(!(ret = waitpid(-1,&status,WNOHANG))){
            sleep(3);
            puts("wait .... \n");
        } 
        if (WIFEXITED(status)){
            printf("pid = [%d] is exited, status=%d\n", ret, WEXITSTATUS(status));
        }
    } 
    
    return 0;
}