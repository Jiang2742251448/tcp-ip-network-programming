#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

int gval = 10;
int main(){

    int lval = 20;
    gval++; // 11
    lval = lval + 5; // 25

    // 创建子进程
    pid_t pid = fork();
    if (pid == 0){ // child
        gval = gval + 2; // 13
        lval = lval + 5; // 30
        printf("child gval is [%d],lval is [%d]\n",gval,lval);
    }else if(pid > 0){ // parent
        gval = gval + 100; // 111
        lval = lval + 100; // 125
        printf("parent gval is [%d],lval is [%d]\n",gval,lval);
    }else{ // error
        fprintf(stderr,"fork error!\n");
        exit(-1);
    }   
    
    return 0;
}