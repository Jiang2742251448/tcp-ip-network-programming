#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(){

    pid_t pid = fork();
    if(pid < 0){
        fprintf(stderr,"fork error!\n");
        exit(1);
    } 

    if(pid == 0){ // child
        return 3;
    }else{ // parent
        pid_t pid2 = fork();
        if(pid < 0){
            fprintf(stderr,"fork error!\n");
            exit(1);
        }
        if (pid2 == 0){
            exit(7);
        }else{
            int status;
            pid_t ret = wait(&status);
            if (WIFEXITED(status)){
                printf("pid = [%d] is exited, status=%d\n", ret, WEXITSTATUS(status));
            }
            ret = wait(&status);
            if (WIFEXITED(status)){
                printf("pid = [%d] is exited, status=%d\n", ret, WEXITSTATUS(status));
            }
            sleep(30);
        }
        
    } 
    
    return 0;
}