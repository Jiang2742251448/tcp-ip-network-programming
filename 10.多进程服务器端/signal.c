#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

void timeout(int n){
    printf("timeout ... \n");
    alarm(2);
}

int main(int argc,char *argv[]){
    // typedef void (*sighandler_t)(int);
    // sighandler_t signal(int signum, sighandler_t handler);

    // 注册一个信号
    signal(SIGALRM,timeout);
    alarm(2);
    while (1){
        sleep(1);
    }
    
    return 0;
}