#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

int main(){

   

    // 创建子进程
    pid_t pid = fork();
    if (pid == 0){ // child
        printf("Child is start!\n");
        printf("Child is over!\n");
    }else if(pid > 0){ // parent
        printf("Parent is start!\n");
        printf("child process id is [%d]\n",pid);
        sleep(30);
        printf("Parent is over!\n");
    }else{ // error
        fprintf(stderr,"fork error!\n");
        exit(-1);
    }   
    


    return 0;
}