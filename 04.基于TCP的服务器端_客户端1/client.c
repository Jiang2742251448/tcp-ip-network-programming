#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <strings.h>
#include <string.h>
#include <ctype.h>


int main(){


    // 监听文件描述符
    int cfd = socket(AF_INET,SOCK_STREAM,0);
    if (cfd < 0){
        fprintf(stderr,"socket error!\n");
        exit(-1);
    }

    // 绑定IP地址和端口号
    struct sockaddr_in caddr;
    // 初始化
    bzero(&caddr,sizeof(caddr));
    caddr.sin_family = AF_INET;
    caddr.sin_port = htons(8888);
    caddr.sin_addr.s_addr = htonl(INADDR_ANY);

    // 连接
    connect(cfd,(struct sockaddr *)&caddr,sizeof(caddr));

    // 读取数据
    char buf[1024];
    int n;
    memset(buf,'\0',sizeof(buf));
    while ((n = read(STDIN_FILENO,buf,sizeof(buf))) > 0){
        // 写
        write(cfd,buf,n);
        memset(buf,'\0',sizeof(buf));
    }
    
    if (n < 0){
        fprintf(stderr,"read error!\n");
        exit(-1);
    }

    // 关闭
    close(cfd);
    
    return 0;
}